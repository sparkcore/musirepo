#include "DualVNH5019MotorShield.h"
#include "pins_arduino.h"

DualVNH5019MotorShield md;

void stopIfFault()
{
  if (md.getM1Fault())
  {
    Serial.println("M1 fault");
    while(1);
  }
  if (md.getM2Fault())
  {
    Serial.println("M2 fault");
    while(1);
  }
}

//update order:
//Right X -> AILE     -> A3
//AUX     -> AUX 1
//Left Y  -> ELEV
//Gear    -> GEAR     -> A4
//Right Y -> THROTTLE -> A2
//Left  X -> RUDD

#define THROTTLE_PIN   A2 // throttle channel (1) from RC receiver
#define STEERING_PIN   A3 // steering channel (4) from RC receiver
#define GEAR_IN_PIN    A4 // gear channel (5) from RC receiver
#define GEAR_OUT_PIN   3  // gear relay pin
#define LED_PIN       13 // user LED pin

#define MAX_SPEED             400 // max motor speed
#define PULSE_WIDTH_DEADBAND   35 // pulse width difference from 1500 us (microseconds) to ignore (to compensate for control centering offset)
#define PULSE_WIDTH_RANGE     350 // pulse width difference from 1500 us to be treated as full scale input (for example, a value of 350 means
                                  //   any pulse width <= 1150 us or >= 1850 us is considered full scale)


void setup()
{
  Serial.begin(115200);
  Serial.println("Dual VNH5019 Motor Shield");
  md.init();
  pinMode(LED_PIN, OUTPUT);
  pinMode(GEAR_OUT_PIN, INPUT);
}

void loop()
{
  int steering = pulseIn(STEERING_PIN, HIGH);
  int throttle = pulseIn(THROTTLE_PIN, HIGH);
  int gear = pulseIn(GEAR_IN_PIN, HIGH);

  int left_speed, right_speed;

  Serial.print("RC: ");
  Serial.print(throttle);
  Serial.print("; ");
  Serial.print(steering);
  Serial.print("; ");
  Serial.println(gear);

  if (throttle > 990 && steering > 0 && gear > 0)
  {
    // both RC signals are good; turn on LED
    digitalWrite(LED_PIN, HIGH);
    
    if(gear > 1850)
    {
      pinMode(GEAR_OUT_PIN, OUTPUT);
    }
    else
    {
      pinMode(GEAR_OUT_PIN, INPUT);
    }
 
    
    // RC signals encode information in pulse width centered on 1500 us (microseconds); subtract 1500 to get a value centered on 0
    throttle -= 1460;
    steering -= 1460;

    // apply deadband
    if (abs(throttle) <= PULSE_WIDTH_DEADBAND)
      throttle = 0;
    if (abs(steering) <= PULSE_WIDTH_DEADBAND)
      steering = 0;

    // mix throttle and steering inputs to obtain left & right motor speeds
    left_speed = ((long)throttle * MAX_SPEED / PULSE_WIDTH_RANGE) - ((long)steering * MAX_SPEED / PULSE_WIDTH_RANGE);
    right_speed = ((long)throttle * MAX_SPEED / PULSE_WIDTH_RANGE) + ((long)steering * MAX_SPEED / PULSE_WIDTH_RANGE);

    // cap speeds to max
    left_speed = min(max(left_speed, -MAX_SPEED), MAX_SPEED);
    right_speed = min(max(right_speed, -MAX_SPEED), MAX_SPEED);
  }
  else
  {
    // at least one RC signal is not good; turn off LED and stop motors
    digitalWrite(LED_PIN, LOW);
    pinMode(GEAR_OUT_PIN, INPUT);

    left_speed = 0;
    right_speed = 0;
  }

  md.setM1Speed(left_speed);
  md.setM2Speed(right_speed);
  stopIfFault();
  Serial.print("Current M1: ");
  Serial.print(md.getM1CurrentMilliamps());
  Serial.print("; M2 : ");
  Serial.println(md.getM2CurrentMilliamps());

}
